﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Xml;
using System.Net;
using System.IO;
using System.Xml.Linq;

namespace Parser
{
    public partial class Form1 : Form
    {

        private static readonly HttpClient client = new HttpClient();
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://uat-tms.openport.com/GPSDataBridgeAPI/encode/auto/gpsdata");
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Method = "POST";
            List<string> listid = new List<string>();
            List<string> listname = new List<string>();
            HttpWebRequest request = null;
            HttpWebResponse response = null;
           StreamReader reader;
            XmlDocument doc = new XmlDocument();
            string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            string id,name,name2="",lat,lon,time;
            string read1;
            string json="[";
            int flag=0;

            // Create the web request  
            request = WebRequest.Create("http://webtrack.naxertech.com/Service/GetAccountDevices/capital/nestle/cmsnestle123") as HttpWebRequest;

            // Get response  
            using (response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
               reader = new StreamReader(response.GetResponseStream());
                
                
                string read=reader.ReadToEnd(); 
                if (read.StartsWith(_byteOrderMarkUtf8))
                {
                    read = read.Remove(0, _byteOrderMarkUtf8.Length);
                }
                read=read.Replace(@"\\",@"");
                 read1 = read.Replace("\\", "").Trim('"').Replace("u000du000a","");
                doc.LoadXml(read1);
            }

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                

                id = node.Attributes["ID"]?.InnerText;
                name = node.Attributes["Name"].InnerText;
                DateTime timeStamp = DateTime.Now.Subtract(new TimeSpan(6, 0, 0));
                int timenow = (int)(timeStamp.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

                request = WebRequest.Create("http://webtrack.naxertech.com/Service/GetDeviceData/capital/nestle/cmsnestle123/" + id + "/" + timenow.ToString()) as HttpWebRequest;

                // Get response  
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    // Get the response stream  
                    reader = new StreamReader(response.GetResponseStream());


                    string read = reader.ReadToEnd();
                    if (read.StartsWith(_byteOrderMarkUtf8))
                    {
                        read = read.Remove(0, _byteOrderMarkUtf8.Length);
                    }
                    read = read.Replace(@"\\", @"");
                    read1 = read.Replace("\\", "").Trim('"').Replace("u000du000a", "");
                    if (read1 != "NILL")
                    {
                        doc.LoadXml(read1);
                        
                        listid.Add(node.Attributes["ID"]?.InnerText);
                        
                        listname.Add(name);
                        name2 = name2 + ", " + name;
                    }
                }
                //string xpath = "/locations/location[2]";

                //XmlNode locationNode = doc.SelectSingleNode(xpath);
                //listBox2.Items.Add(locationNode.Attributes["Lat"]?.InnerText);
                if (read1 != "NILL")
                {
                    foreach (XmlNode node1 in doc.DocumentElement.ChildNodes)
                    {
                        if (node1.Attributes["Lat"].InnerText != "0" && node1.Attributes["Lon"].InnerText != "0")
                        {
                            lat = node1.Attributes["Lat"]?.InnerText;
                            lon = node1.Attributes["Lon"]?.InnerText;
                            listBox2.Items.Add(lat);
                            double unixTimeStamp = double.Parse(node1.Attributes["Time"]?.InnerText);

                            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
                            time = dtDateTime.ToString("dddd, d MMMM yyy hh:mm:ss tt");
                            listBox4.Items.Add(time);
                           
                           
                            json = json + "{\"latitude\":" + lat + ",\"longitude\":" + lon + ",\"poll_time\":\"" + time + "\",\"equipment_nbr\":\"" + name + "\"},";
                            flag = 1;
                        }
                    }



                   


                }
            }
           if(flag==1)
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://uat-tms.openport.com/GPSDataBridgeAPI/encode/auto/gpsdatalist");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    json = json.Remove(json.Length - 1);
                    json = json + "]";

                    //json = " {\"latitude\":75.65,\"longitude\":96.54,\"equipment_nbr\":2210}";
                    listBox6.Items.Add(json);


                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }


                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    listBox5.Items.Add(result);
                }



            }
            listBox1.DataSource = listid;
            listBox3.DataSource = listname;
            textBox2.Text = listBox2.Items.Count.ToString();
            textBox3.Text = listBox3.Items.Count.ToString();
            textBox4.Text = name2;
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            

           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WebRequest request = WebRequest.Create("http://webtrack.naxertech.com/Service/GetDeviceData/apidemo/apidemo/elevaTe23G/1011239/1") as HttpWebRequest;
            try
            {
                WebResponse response = request.GetResponse() as HttpWebResponse;
                using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    XDocument xmlDoc = new XDocument();
                    try
                    {
                        xmlDoc = XDocument.Parse(sr.ReadToEnd());
                        string status = xmlDoc.Root.Element("locations").Attribute("Ana").Value;
                        textBox1.Text = status;
                        Console.WriteLine("Response status: {0}", status);
                        if (status == "OK")
                        {
                            // if the status is OK it's normally safe to assume the required elements
                            // are there. However, if you want to be safe you can always check the element
                            // exists before retrieving the value
                            Console.WriteLine(xmlDoc.Root.Element("CountryCode").Value);
                            Console.WriteLine(xmlDoc.Root.Element("CountryName").Value);
             
            }
                    }
                    catch (Exception)
                    {
                        // handle if necessary
                    }
                }
            }
            catch (WebException)
            {
                // handle if necessary    
            }
        }
    }
}
